# Use the official Node.js image.
# https://hub.docker.com/_/node
FROM node:16.20.2-alpine

# Create and change to the app directory.
WORKDIR /usr/src/app

# Copy the package.json and yarn.lock files.
COPY package.json yarn.lock ./

# Install dependencies.
RUN yarn install

# Copy the rest of the application files.
COPY . .

# Build the React app.
RUN yarn build

# Expose the port the app runs on
EXPOSE 3000

# Start the app.
CMD ["yarn", "start"]