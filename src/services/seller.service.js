import { useQuery, useMutation } from "react-query";
import httpRequestAuth from "./httpRequestAuth";

const useSellerService = {
  getList: (params, tableSlug) => httpRequestAuth.get(tableSlug, { params: params }),
  getByID: (id, params) => httpRequestAuth.get(`seller/${id}`, { params }),
  create: (data) => httpRequestAuth.post("seller", data),
  update: (data) => httpRequestAuth.put("seller", data),
  delete: ({ pathname, data }) => httpRequestAuth.delete(pathname, { data }),
};

export const useGetAllSellerList = ({ params = {}, tableSlug } = {}) => {
  return useQuery(["SELLER", params, tableSlug], () => {
    return useSellerService.getList(params, tableSlug);
  });
};

export const useGetByIdQuery = ({ id, params = {}, queryParams }) => {
  return useQuery(
    ["SELLER_BY_ID", { id, ...params }],
    () => {
      return useSellerService.getByID(id, params);
    },
    queryParams,
  );
};

export const useCreateMutation = (mutation) => {
  return useMutation((data) => useSellerService.create(data), mutation);
};

export const useUpdateMutation = (mutation) => {
  return useMutation((data) => useSellerService.update(data), mutation);
};

export const useDeleteMutation = (mutation) => {
  return useMutation((mypathRemove) => useSellerService.delete(mypathRemove), mutation);
};