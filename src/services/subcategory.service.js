import { useQuery, useMutation } from "react-query";
import httpRequestAuth from "./httpRequestAuth";

const useSubCategoryService = {
  getList: (params, tableSlug) => httpRequestAuth.get(tableSlug, { params: params }),
  getByID: (id, params) => httpRequestAuth.get(`category/${id}`, { params }),
  create: (data) => httpRequestAuth.post(`category?only_sub=true`, data),
  update: (data) => httpRequestAuth.put(`subcategory`, data),
  delete: ({ pathname, data }) => httpRequestAuth.delete(pathname, { data }),
};

export const useGetAllSubCategoryList = ({ params = {}, tableSlug } = {}) => {
  return useQuery(["SUBCATEGORY", params, tableSlug], () => {
    return useSubCategoryService.getList(params, tableSlug);
  });
};

export const useGetByIdQuery = ({ id, params = {}, queryParams }) => {
  return useQuery(
    ["SUBCATEGORY_BY_ID", { id, ...params }],
    () => {
      return useSubCategoryService.getByID(id, params);
    },
    queryParams,
  );
};

export const useCreateMutation = (mutation) => {
  return useMutation((data) => useSubCategoryService.create(data), mutation);
};

export const useUpdateMutation = (mutation) => {
  return useMutation((data) => useSubCategoryService.update(data), mutation);
};

export const useDeleteMutation = (mutation) => {
  return useMutation((mypathRemove) => useSubCategoryService.delete(mypathRemove), mutation);
};
