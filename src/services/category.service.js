import { useQuery, useMutation } from "react-query";
import httpRequestAuth from "./httpRequestAuth";

const useCategoryService = {
  getList: (params, tableSlug) => httpRequestAuth.get(tableSlug, { params: params }),
  getByID: (id, params) => httpRequestAuth.get(`category/${id}`, { params }),
  create: (data) => httpRequestAuth.post("category", data),
  update: (data) => httpRequestAuth.put("category", data),
  delete: ({ pathname, data }) => httpRequestAuth.delete(pathname, { data }),
};

export const useGetAllCategoryList = ({ params = {}, tableSlug } = {}) => {
  return useQuery(["CATEGORY", params, tableSlug], () => {
    return useCategoryService.getList(params, tableSlug);
  });
};

export const useGetByIdQuery = ({ id, params = {}, queryParams }) => {
  return useQuery(
    ["CATEGORY_BY_ID", { id, ...params }],
    () => {
      return useCategoryService.getByID(id, params);
    },
    queryParams,
  );
};

export const useCreateMutation = (mutation) => {
  return useMutation((data) => useCategoryService.create(data), mutation);
};

export const useUpdateMutation = (mutation) => {
  return useMutation((data) => useCategoryService.update(data), mutation);
};

export const useDeleteMutation = (mutation) => {
  return useMutation((mypathRemove) => useCategoryService.delete(mypathRemove), mutation);
};
