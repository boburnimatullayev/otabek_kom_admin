import { useQuery, useMutation } from "react-query";
import httpRequestAuth from "./httpRequestAuth";

const useBrandService = {
  getList: (params, tableSlug) => httpRequestAuth.get(tableSlug, { params: params }),
  getByID: (id, params) => httpRequestAuth.get(`brand/${id}`, { params }),
  create: (data) => httpRequestAuth.post("brand", data),
  update: (data) => httpRequestAuth.put(`brand`, data),
  delete: ({ pathname, data }) => httpRequestAuth.delete(pathname, { data }),
};

export const useGetAllBrandList = ({ params = {}, tableSlug } = {}) => {
  return useQuery(["BRAND", params, tableSlug], () => {
    return useBrandService.getList(params, tableSlug);
  });
};

export const useGetByIdQuery = ({ id, params = {}, queryParams }) => {
  return useQuery(
    ["BRAND_BY_ID", { id, ...params }],
    () => {
      return useBrandService.getByID(id, params);
    },
    queryParams,
  );
};

export const useCreateMutation = (mutation) => {
  return useMutation((data) => useBrandService.create(data), mutation);
};

export const useUpdateMutation = (mutation) => {
  return useMutation((data) => useBrandService.update(data), mutation);
};

export const useDeleteMutation = (mutation) => {
  return useMutation((mypathRemove) => useBrandService.delete(mypathRemove), mutation);
};
