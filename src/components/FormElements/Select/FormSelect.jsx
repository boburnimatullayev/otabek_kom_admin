import { Select } from 'chakra-react-select';
import { Controller } from 'react-hook-form';
import formatGroupLabel from './formatGroupLabel';


const FormSelect = ({
	control,
	name,
	options = [],
	disabled,
	defaultValue = '',
  className,
  
	customOnChange = () => {},
	required,
	...props
}) => {
	return (
		<Controller
			name={name}
			control={control}
			defaultValue={defaultValue}
			render={({ field: { onChange, value } }) => (
				<Select
				
					options={options}
          className={className}
					isReadOnly={disabled}
					// components={{ Option: IconOption, SingleValue: CustomSelectValue }}
					value={options.find((option) => option.value === value)}
					onChange={(val) => {
						onChange(val.value);
						customOnChange(val);
					}}
				  chakraStyles={{
						dropdownIndicator: (prev, { selectProps: { menuIsOpen } }) => ({
							...prev,
							'> svg': {
								transitionDuration: 'normal',
								transform: `rotate(${menuIsOpen ? -180 : 0}deg)`
							}
						}),
            
					}}
					formatGroupLabel={formatGroupLabel}
					{...props}
					size="lg"
					required={required}
				/>
			)}
		/>
	);
};

export default FormSelect;
