import { Box, Button, Flex, Heading } from "@chakra-ui/react";
import BackButton from "components/BackButton";
import Header, { HeaderExtraSide, HeaderLeftSide, HeaderTitle } from "components/Header";
import { Page } from "components/Page";
import PageCard, { PageCardHeader } from "components/PageCard";
import useCustomToast from "hooks/useCustomToast";
import { useForm } from "react-hook-form";
import { useNavigate, useParams } from "react-router-dom";
import queryClient from "services/queryClient";
import { useGetRoadmapByIdQuery, useRoadmapCreateMutation, useRoadmapUpdateMutation } from "services/roadmap.service";
import Flow from "modules/Roadmap/ReactFlow/Flow";
import { useState } from "react";
import { useNodesState, useEdgesState, handleParentExpand } from "reactflow";
import FormRow from "components/FormElements/FormRow";
import FormInput from "components/FormElements/Input/FormInput";
import FormSelect from "components/FormElements/Select/FormSelect";
import { useGetAllCategoryList } from "services/category.service";

import styles from "./index.module.scss";
import Textarea from "components/FormElements/Input/TextArea";
const initialNodes = [
  {
    id: "1",
    type: "initial",
    position: { x: 250, y: 5 },
    data: {
      value: "",
      discription: "",
    },
  },
];

export default function RoadmapCards() {
  const [nodes, setNodes, onNodesChange] = useNodesState(initialNodes);
  const [edges, setEdges, onEdgesChange] = useEdgesState([]);
  const { id } = useParams();
  const navigate = useNavigate();
  const { handleSubmit, control, reset } = useForm({});
  const { successToast } = useCustomToast();

  const { data } = useGetAllDepartmentList({
    params: {
      limit: 10,
      offset: 0,
    },
    tableSlug: "/department",
  });

  const { mutate: createRoadmap, isLoading } = useRoadmapCreateMutation({
    onSuccess: () => {
      queryClient.refetchQueries("DEPARTMENT");
      successToast();
      navigate(-1);
    },
  });

  const { data: roadmap } = useGetRoadmapByIdQuery({
    id: id,
    queryParams: {
      cacheTime: false,
      enabled: Boolean(id),
      onSuccess: (res) => {
        reset(res);
        const actions = JSON.parse(res.actions);
        setNodes(actions.nodes);
        setEdges(actions.edges);
      },
    },
  });

  const { mutate: update, isLoading: updateLoading } = useRoadmapUpdateMutation({
    onSuccess: () => {
      successToast();
      navigate(-1);
    },
  });

  const onSubmit = (values) => {
    const createData = {
      title: values.title,
      description: values.description,
      department_id: values.department_id,
      actions: JSON.stringify({
        nodes,
        edges,
      }),
      short_link: values.short_link,
    };
    if (id) {
      update({
        id,
        ...createData,
      });
    } else {
      createRoadmap(createData);
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Header>
        <HeaderLeftSide ml={"-40px"}>
          <BackButton />
          <HeaderTitle>{id ? roadmap?.title : "Добавить roadmap"}</HeaderTitle>
        </HeaderLeftSide>
        <HeaderExtraSide>
          <Button type="submit" ml="auto" isLoading={updateLoading || isLoading}>
            Сохранить
          </Button>
        </HeaderExtraSide>
      </Header>
      <Box borderRadius={"6px"} display={"flex"} flexDirection={"column"} justifyContent={"center"} p={4}>
        <Page>
          <PageCard w="100%">
            <Box style={{ padding: "20px 20px  10px 20px" }}>
              <Flex gap={10}>
                <FormRow label="Заголовок:" required>
                  <FormInput
                    control={control}
                    name="title"
                    placeholder="Введите title пользователя"
                    autoFocus
                    validation={{
                      required: {
                        value: true,
                        message: "Обязательное поле",
                      },
                    }}
                  />
                </FormRow>
                <FormRow label="Отделение" required>
                  <FormSelect
                    options={data?.departments?.map((el) => ({ label: el?.name, value: el?.id }))}
                    control={control}
                    name="department_id"
                    placeholder="Введите отделение"
                    // required
                  />
                </FormRow>
                <FormRow label="Link" required>
                  <FormInput
                    control={control}
                    name="short_link"
                    placeholder="Введите short_link"
                    autoFocus
                    validation={{
                      required: {
                        value: true,
                        message: "Обязательное поле",
                      },
                    }}
                  />
                </FormRow>
                <FormRow label="Описание:" required>
                  <Textarea
                    className={styles.textarea}
                    control={control}
                    name="description"
                    placeholder="Введите oписание"
                    required
                  />
                </FormRow>
              </Flex>
            </Box>
            <PageCardHeader>
              <HeaderLeftSide>
                <Heading fontSize="xl">Данные o roadmap</Heading>
              </HeaderLeftSide>
            </PageCardHeader>
            <Page>
              <PageCard>
                <Flow
                  nodes={nodes}
                  setNodes={setNodes}
                  onNodesChange={onNodesChange}
                  edges={edges}
                  setEdges={setEdges}
                  onEdgesChange={onEdgesChange}
                />
              </PageCard>
            </Page>
          </PageCard>
        </Page>
      </Box>
    </form>
  );
}
