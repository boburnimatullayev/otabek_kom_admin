import styles from "./style.module.scss";
import { Handle, Position } from "reactflow";

export default function Initial({ data, setNodes, id }) {
  const onChange = async (e) => {
    setNodes((prev) =>
      prev.map((node) =>
        node.id === id
          ? {
              ...node,
              data: {
                ...node.data,
                value: e.target.value,
              },
            }
          : node,
      ),
    );
  };

  return (
    <div className={styles.initial}>
      <input onChange={onChange} value={data?.value} placeholder="Enter something...." />
      <Handle type="source" id="bottomNode" position={Position.Bottom} />
    </div>
  );
}
