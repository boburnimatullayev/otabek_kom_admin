import 'react-quill/dist/quill.snow.css';
import { useMemo } from 'react';
import ReactQuill from 'react-quill';

const Editor = ({handleChange,valueDis}) => {
	const customModules = useMemo(() => {
		return {
			toolbar: {
				container: [
					['bold', 'italic', 'underline', 'strike'],
					['blockquote', 'code-block'],
					[{ header: 1 }, { header: 2 }],
					[{ list: 'ordered' }, { list: 'bullet' }],
					[{ script: 'sub' }, { script: 'super' }],
					[{ indent: '-1' }, { indent: '+1' }],
					[{ header: [1, 2, 3, 4, 5, 6, false] }],
					[{ color: [] }, { background: [] }],
					[{ font: [] }],
					[{ align: [] }],
				],
			},
		};
	}, []);

	return (
		<div className='reactQuill'>
			<ReactQuill
				theme="snow"
				modules={customModules}
				value={valueDis}
				placeholder="Type description"
				onChange = {handleChange}
			/>
		</div>
	);
};

export default Editor;