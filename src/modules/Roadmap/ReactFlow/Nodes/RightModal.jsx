import { Input, Heading, Text, Flex } from "@chakra-ui/react";
import styles from "./style.modal.scss";
import Editor from "./ReactQuill";
import { CloseIcon } from "@chakra-ui/icons";

const ModalSidebar = ({ open, setNodes, nodes, currentItem, onClose }) => {
  const currentNode = nodes?.find((item) => item.id === currentItem?.id);

  const onChange = (e) => {
    setNodes((prev) =>
      prev.map((node) =>
        currentNode.id === node.id
          ? {
              ...node,
              data: {
                ...node.data,
                value: e.target.value,
              },
            }
          : node,
      ),
    );
  };

  const handleChange = (e) => {
    if (e !== "<p><br></p>") {
      setNodes((prev) =>
        prev.map((node) =>
          currentNode.id === node.id
            ? {
                ...node,
                data: {
                  ...node.data,
                  description: e,
                },
              }
            : node,
        ),
      );
    }
  };

  return (
    <>
      {open ? (
        <div className="modal-container">
          <div
            className="modal-content"
            style={{
              boxShadow: "none",
            }}
          >
            <Flex alignItems="center" mb={4} justifyContent="space-between">
              <Heading fontSize="xl">Details</Heading>
              <CloseIcon cursor="pointer" onClick={onClose} />
            </Flex>
            <div className="modal-body">
              <Text mb="8px">Title</Text>
              <Input
                value={currentNode?.data?.value}
                onChange={onChange}
                className={styles.titleInput}
                placeholder="Type title"
                mb={5}
              />
              <Text mb="8px">Description</Text>
              <Editor valueDis={currentNode?.data?.description} handleChange={handleChange} />
              {/* <div className={styles.positions}>
                <Stack align="left" direction="column" gap="25px">
                  <div>
                    <p className={styles.posTitles}>Top</p>
                    <Switch
                      onChange={(e) => swichOnchange(e, "top")}
                      isChecked={currentItem?.data?.top === "target" || false}
                      size="lg"
                    />{" "}
                    {currentItem?.data?.top}
                  </div>
                  <div>
                    <p className={styles.posTitles}>Bottom</p>
                    <Switch
                      onChange={(e) => swichOnchange(e, "bottom")}
                      isChecked={currentItem?.data?.bottom === "target" || false}
                      size="lg"
                    />
                    {currentItem?.data?.bottom}
                  </div>
                  <div>
                    <p className={styles.posTitles}>Right</p>
                    <Switch
                      onChange={(e) => swichOnchange(e, "right")}
                      isChecked={currentItem?.data?.right === "target" || false}
                      size="lg"
                    />{" "}
                    {currentItem?.data?.right}
                  </div>

                  <div>
                    <p className={styles.posTitles}>Left</p>
                    <Switch
                      onChange={(e) => swichOnchange(e, "left")}
                      isChecked={currentItem?.data?.left === "target" || false}
                      size="lg"
                    />{" "}
                    {currentItem?.data?.left}
                  </div>
                </Stack>
              </div> */}
            </div>
          </div>
        </div>
      ) : (
        <></>
      )}
    </>
  );
};

export default ModalSidebar;
