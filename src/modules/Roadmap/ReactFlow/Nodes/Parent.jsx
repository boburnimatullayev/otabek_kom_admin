import styles from "./style.module.scss";
import { Handle, Position } from "reactflow";

export default function Parent({ data, setNodes, id }) {
  const onChange = async (e) => {
    setNodes((prev) =>
      prev.map((node) =>
        node.id === id
          ? {
              ...node,
              data: {
                ...node.data,
                value: e.target.value,
              },
            }
          : node,
      ),
    );
  };

  return (
    <>
      <div className={styles.parent}>
        <Handle
          type="source"
          style={{
            top: "70%",
            background: "#4262ff",
          }}
          id={`leftNode${id}`}
          position={Position.Left}
        />
        <Handle
          type="target"
          style={{
            top: "30%",
            background: "#ae2a1b",
          }}
          id={`leftNode${id}2`}
          position={Position.Left}
        />
        <Handle
          style={{
            left: "55%",
            background: "#4262ff",
          }}
          type="source"
          id={`topNode${id}`}
          position={Position.Top}
        />
        <Handle
          type="target"
          style={{
            left: "45%",
            background: "#ae2a1b",
          }}
          id={`topNode${id}2`}
          position={Position.Top}
        />
        <input onChange={onChange} placeholder="Enter something...." value={data?.value} />
        <Handle
          type="source"
          style={{
            top: "70%",
            background: "#4262ff",
          }}
          id={`rightNode${id}`}
          position={Position.Right}
        />
        <Handle
          type="target"
          id={`rightNode${data.id}2`}
          position={Position.Right}
          style={{
            top: "30%",
            background: "#ae2a1b",
          }}
        />
        <Handle
          type="target"
          style={{
            left: "45%",
            background: "#ae2a1b",
          }}
          id={`bottomNode${id}2`}
          position={Position.Bottom}
        />
        <Handle
          style={{
            left: "55%",
            background: "#4262ff",
          }}
          type="source"
          id={`bottomNode${id}`}
          position={Position.Bottom}
        />
      </div>
    </>
  );
}
