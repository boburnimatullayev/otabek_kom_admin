import { useState, useRef, useCallback, useMemo } from "react";
import ReactFlow, { ReactFlowProvider, addEdge } from "reactflow";
import "reactflow/dist/style.css";
import Sidebar from "./Sidebar";
import styles from "./style.module.scss";
import Parent from "./Nodes/Parent";
import Children from "./Nodes/Children";
import Initial from "./Nodes/Initial";
import WithProps from "../../../HOC/FlowProps";
import ModalSidebar from "./Nodes/RightModal";

function generateId() {
  return new Date().getTime().toString();
}

const Flow = ({ nodes, setNodes, onNodesChange, edges, setEdges, onEdgesChange }) => {
  const reactFlowWrapper = useRef(null);
  const [reactFlowInstance, setReactFlowInstance] = useState(null);
  const [menu, setMenu] = useState(null);
  const [currentItem, setCurrentItem] = useState();
  const [open, setOpen] = useState(false);

  const cloneInputType = ({ params }) => {
    const node = nodes.find((item) => item.id === params.target);
    const lineType = node?.type === "parent";
    return {
      ...params,
      animated: !lineType,
      style: { stroke: "#2b77e4", strokeWidth: 2 },
      type: lineType ? "step" : "BezierEdge",
    };
  };

  const onConnect = useCallback(
    (params) => setEdges((eds) => addEdge(cloneInputType({ params }), eds)),
    [nodes, edges],
  );

  const onDragOver = useCallback((event) => {
    event.preventDefault();

    event.dataTransfer.dropEffect = "move";
  }, []);

  const onDrop = useCallback(
    (event) => {
      event.preventDefault();
      const reactFlowBounds = reactFlowWrapper.current.getBoundingClientRect();
      const type = event.dataTransfer.getData("application/reactflow");

      if (typeof type === "undefined" || !type) {
        return;
      }

      const position = reactFlowInstance.project({
        x: event.clientX - reactFlowBounds.left,
        y: event.clientY - reactFlowBounds.top,
      });
      const positionAbsolute = reactFlowInstance.project({
        x: event.clientX - reactFlowBounds.left,
        y: event.clientY - reactFlowBounds.top,
      });

      const newNode = {
        id: generateId(),
        type,
        position,
        positionAbsolute,
        data: {
          value: "",
          discription: "",
          top: "source",
          left: "source",
          bottom: "source",
          right: "source",
        },
      };

      setNodes((nds) => nds.concat(newNode));
    },
    [reactFlowInstance],
  );

  const nodeTypes = useMemo(
    () => ({
      parent: WithProps(Parent, {
        setNodes,
        setEdges,
      }),
      children: WithProps(Children, {
        setNodes,
        setEdges,
      }),
      initial: WithProps(Initial, {
        setNodes,
        setEdges,
      }),
    }),
    [setNodes, setEdges, edges],
  );

  const onPaneClick = useCallback(
    (e) => {
      setMenu(null);
    },
    [setMenu],
  );

  const getCurrentItem = (event, element) => {
    if (element.id !== "1") {
      setCurrentItem(element);
      setOpen(true);
    } else {
      setOpen(false);
    }
  };

  const deletedItem = (id) => {
    const deletedElement = nodes?.filter((el) => el.id !== id);
    setNodes(deletedElement);
  };

  const handleclick = (e) => {
    if (
      e.target instanceof HTMLInputElement ||
      e.target.className === "_parent_19fk6_1" ||
      e.target.className === "_parent_19fk6_2"
    ) {
    } else {
      setOpen(false);
    }
  };

  const onCloseSidebar = () => {
    setOpen(false);
    setCurrentItem(null);
  };
  const onZoom = (event) => {
    event.preventDefault(); // Disable zooming
  };

  return (
    <div className={styles.main}>
      <ReactFlowProvider>
        <div className={styles.flow}>
          <Sidebar />
          <div className={styles.reactFlow} ref={reactFlowWrapper}>
            <ReactFlow
              nodes={nodes}
              edges={edges}
              onNodesChange={onNodesChange}
              onEdgesChange={onEdgesChange}
              onConnect={onConnect}
              onInit={setReactFlowInstance}
              onDrop={onDrop}
              onDragOver={onDragOver}
              nodeTypes={nodeTypes}
              // panOnScroll
              fitView
              positionAbsolute
              onPaneClick={onPaneClick}
              onNodeClick={getCurrentItem}
              onNodesDelete={deletedItem}
              onClick={handleclick}
              maxZoom={1}
              minZoom={1}
            />
          </div>

          <ModalSidebar
            onClose={onCloseSidebar}
            open={open}
            setNodes={setNodes}
            currentItem={currentItem}
            nodes={nodes}
          />
        </div>
      </ReactFlowProvider>
    </div>
  );
};

export default Flow;
