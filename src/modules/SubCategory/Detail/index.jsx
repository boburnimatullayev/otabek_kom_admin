import { Button, Heading } from "@chakra-ui/react";
import { useForm } from "react-hook-form";
import { useNavigate, useParams } from "react-router-dom";
import BackButton from "../../../components/BackButton";
import FormRow from "../../../components/FormElements/FormRow";
import FormInput from "../../../components/FormElements/Input/FormInput";
import Header, { HeaderExtraSide, HeaderLeftSide, HeaderTitle } from "../../../components/Header";
import SimpleLoader from "../../../components/Loaders/SimpleLoader";
import NotificationMenu from "../../../components/NotificationMenu";
import { Page } from "../../../components/Page";
import PageCard, { PageCardFooter, PageCardForm, PageCardHeader } from "../../../components/PageCard";
import ProfileMenu from "../../../components/ProfileMenu";
import useCustomToast from "../../../hooks/useCustomToast";
import Textarea from "components/FormElements/Input/TextArea";
import cls from "./Detail.module.scss";
import { useUpdateMutation, useGetByIdQuery } from "services/subcategory.service";
import FormSelect from "components/FormElements/Select/FormSelect";
import { useGetAllCategoryList } from "services/category.service";
import { values } from "mobx";

const SubCategoryDetail = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const { successToast } = useCustomToast();

  const { data } = useGetAllCategoryList({
    params: {
      limit: 10,
      offset: 0,
    },
    tableSlug: "/category",
  });

  const { control, reset, handleSubmit } = useForm({
    defaultValues: {},
  });

  const { isLoading, data: subCategory } = useGetByIdQuery({
    id: id,
    queryParams: {
      cacheTime: false,
      enabled: Boolean(id),
      onSuccess: reset,
    },
  });

  const { mutate: updateSubCategory, isLoading: updateLoading } = useUpdateMutation({
    onSuccess: () => {
      successToast();
      navigate(-1);
    },
  });

  const onSubmit = (values) => {
    const updateData = {
      name: values.name,
      parent_id: values.parent_id,
    };
    updateSubCategory({
      id: id,
      ...updateData,
    });
  };

  if (isLoading) return <SimpleLoader h="100vh" />;

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Header>
        <HeaderLeftSide ml={-10}>
          <BackButton />
          <HeaderTitle>Обозначение</HeaderTitle>
        </HeaderLeftSide>
        <HeaderExtraSide>
          {/* <NotificationMenu /> */}
          {/* <ProfileMenu /> */}
        </HeaderExtraSide>
      </Header>

      <Page p={4} h="100% !important">
        <PageCard w={600}>
          <PageCardHeader>
            <HeaderLeftSide>
              <Heading fontSize="xl">Все данные</Heading>
            </HeaderLeftSide>
          </PageCardHeader>

          <PageCardForm p={6} spacing={8}>
            <FormRow label="Отделение">
              <FormSelect
                options={data?.map((el) => ({ label: el?.name, value: el?.id }))}
                control={control}
                name="parent_id"
                placeholder="Введите отделение"
              />
            </FormRow>
            <FormRow label="Название:">
              <FormInput control={control} name="name" placeholder="Введите название" autoFocus required />
            </FormRow>
          </PageCardForm>

          <PageCardFooter mt={6}>
            <Button isLoading={updateLoading} type="submit" ml="auto">
              Сохранить
            </Button>
          </PageCardFooter>
        </PageCard>
      </Page>
    </form>
  );
};
export default SubCategoryDetail;
