import { Box, Button, Heading } from "@chakra-ui/react";
import BackButton from "components/BackButton";
import FormRow from "components/FormElements/FormRow";
import FormInput from "components/FormElements/Input/FormInput";
import Textarea from "components/FormElements/Input/TextArea";
import FormSelect from "components/FormElements/Select/FormSelect";
import Header, { HeaderExtraSide, HeaderLeftSide, HeaderTitle } from "components/Header";
import { Page } from "components/Page";
import PageCard, { PageCardFooter, PageCardForm, PageCardHeader } from "components/PageCard";
import useCustomToast from "hooks/useCustomToast";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { useGetAllCategoryList } from "services/category.service";
import { useCreateMutation } from "services/subcategory.service";
import queryClient from "services/queryClient";
import cls from "./Detail.module.scss";

export default function SubCategoryCards() {
  const navigate = useNavigate();
  const { control, handleSubmit } = useForm({});
  const { successToast } = useCustomToast();

  const { data } = useGetAllCategoryList({
    params: {
      limit: 10,
      offset: 0,
    },
    tableSlug: "/category",
  });

  const { mutate: createSubCategory } = useCreateMutation({
    onSuccess: () => {
      queryClient.refetchQueries("DEPARTMENT");
      successToast();
      navigate(-1);
    },
  });

  const onSubmit = (values) => {
    const createData = {
      name: values.name,
      // description: values.description,
		parent_id: values.parent_id,
    };

    createSubCategory(createData);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Header>
        <HeaderLeftSide ml={"-40px"}>
          <BackButton />
          <HeaderTitle>Добавить обозначение</HeaderTitle>
        </HeaderLeftSide>
        <HeaderExtraSide></HeaderExtraSide>
      </Header>
      <Box borderRadius={"6px"} display={"flex"} flexDirection={"column"} justifyContent={"center"} p={4}>
        <Page>
          <PageCard w={600}>
            <PageCardHeader>
              <HeaderLeftSide>
                <Heading fontSize="xl">Данные об обозначение</Heading>
              </HeaderLeftSide>
            </PageCardHeader>

            <PageCardForm p={6} spacing={8} h="100%">
              <FormRow label="Отделение" required>
                <FormSelect
                  options={data?.map((el) => ({ label: el?.name, value: el?.id }))}
                  control={control}
						name="parent_id"
                  placeholder="Введите отделение"
                  required
                />
              </FormRow>
              <FormRow label="Имя:" required>
                <FormInput
                  control={control}
                  name="name"
                  placeholder="Введите имя пользователя"
                  autoFocus
                  validation={{
                    required: {
                      value: true,
                      message: "Обязательное поле",
                    },
                  }}
                />
              </FormRow>
            </PageCardForm>

            <PageCardFooter mt={6}>
              <Button type="submit" ml="auto">
                Сохранить
              </Button>
            </PageCardFooter>
          </PageCard>
        </Page>
      </Box>
    </form>
  );
}
