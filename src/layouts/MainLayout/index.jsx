import { Box, Flex } from "@chakra-ui/react";
import Sidebar from "../../components/Sidebar";
import { FiUser } from "react-icons/fi";
import { BiHomeSmile } from "react-icons/bi";
import { MdDesignServices } from "react-icons/md";
import { HiOutlineUsers } from "react-icons/hi2";
import { Outlet } from "react-router-dom";

const elements = [
  {
    label: "Kategories",
    icon: BiHomeSmile,
    link: "/category",
  },
  {
    label: "Subkategories",
    icon: MdDesignServices,
    link: "/subcategory",
  },
  {
    label: "Brend",
    icon: MdDesignServices,
    link: "/brand",
  },
  {
    label: "Sellers",
    icon: FiUser,
    link: "/seller",
  },
  {
    label: "Роли",
    icon: HiOutlineUsers,
    link: "/role",
  },
  {
    label: "Roadmaps",
    icon: MdDesignServices,
    link: "/roadmap",
  },
];

const MainLayout = () => {
  return (
    <Flex>
      <Box position="sticky" top={0} height="100vh" overflowY="auto">
        <Sidebar elements={elements} />
      </Box>

      <Box background="#f0f0f3" flex={1} height="100vh" overflowY="auto">
        <Outlet />
      </Box>
    </Flex>
  );
};
export default MainLayout;
