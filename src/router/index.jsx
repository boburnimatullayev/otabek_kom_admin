import { observer } from "mobx-react-lite";
import { Navigate, Route, Routes } from "react-router-dom";
import AuthLayout from "../layouts/AuthLayout";
import MainLayout from "../layouts/MainLayout";
import Login from "../modules/Login";
import authStore from "../store/auth.store";
import Category from "../modules/Category";
import Cards from "modules/Category/components/Cards";
import CategoryDetail from "modules/Category/Detail";
import SubCategory from "modules/SubCategory/List";
import SubCategoryCards from "modules/SubCategory/components/Cards";
import SubCategoryDetail from "modules/SubCategory/Detail";
import Brand from "modules/Brand";
import CardsBrand from "modules/Brand/components/Cards";
import BrandDetail from "modules/Brand/Detail";
import SellerList from "modules/Seller/List";
import SellerCards from "modules/Seller/components/Cards";
import SellerDetail from "modules/Seller/Detail";
import Roles from "modules/Roles/List";
import RolesCards from "modules/Roles/components/Cards";
import RoleDetail from "modules/Roles/Detail";
import Roadmaps from "modules/Roadmap/List";
import RoadmapCards from "modules/Roadmap/components/Cards";
import RoadmapDetail from "modules/Roadmap/Detail";

const Router = () => {
  const { isAuth } = authStore;

  if (!isAuth)
    return (
      <Routes>
        <Route path="/" element={<AuthLayout />}>
          <Route index element={<Navigate to="/login " />} />
          <Route path="login" element={<Login />} />
          <Route path="*" element={<Navigate to="/login" />} />
        </Route>
        <Route path="*" element={<Navigate to="/login" />} />
      </Routes>
    );

  return (
    <Routes>
      <Route path="/" element={<MainLayout />}>
        <Route index element={<Navigate to="/category" />} />

        <Route path="/category" element={<Category />} />
        <Route path="category/create" element={<Cards />} />
        <Route path="category/:id" element={<CategoryDetail />} />
        <Route path="subcategory" element={<SubCategory />} />
        <Route path="subcategory/create" element={<SubCategoryCards />} />
        <Route path="subcategory/:id" element={<SubCategoryDetail />} />
        <Route path="brand" element={<Brand />} />
        <Route path="brand/create" element={<CardsBrand />} />
        <Route path="brand/:id" element={<BrandDetail />} />
        <Route path="seller" element={<SellerList />} />
        <Route path="seller/create" element={<SellerCards />} />
        <Route path="seller/:id" element={<SellerDetail />} />
        <Route path="role" element={<Roles />} />
        <Route path="role/create" element={<RolesCards />} />
        <Route path="role/:id" element={<RoleDetail />} />
        <Route path="roadmap" element={<Roadmaps />} />
        <Route path="roadmap/create" element={<RoadmapCards />} />
        <Route path="roadmap/:id" element={<RoadmapCards />} />

        <Route path="*" element={<Navigate to="/category" />} />
      </Route>

      <Route path="*" element={<Navigate to="/category" />} />
    </Routes>
  );
};

export default observer(Router);
