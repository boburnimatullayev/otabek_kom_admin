import { makeAutoObservable } from "mobx";
import { makePersistable } from "mobx-persist-store";

class Store {
  constructor() {
    makeAutoObservable(this);

    makePersistable(this, {
      name: "authStore",
      properties: ["isAuth", "userData", "token", "access_token", "refresh_token"],
      storage: window.localStorage,
    });
  }

  isAuth = false;
  userData = {};
  token = {};
  // access_token = {};
  // refresh_token = {};

  setIsAuth(value) {
    this.isAuth = value;
  }

  login(data) {
    this.isAuth = true;
    this.userData = data.user;
    this.token = {
      access_token: data.access_token,
    };
    // this.access_token = data.access_token;
    // this.refresh_token = data.refresh_token;
  }

  logout() {
    this.isAuth = false;
    this.userData = {};
    this.token = {};
    // this.access_token = {};
    // this.refresh_token = {};
  }
}

const authStore = new Store();
export default authStore;
